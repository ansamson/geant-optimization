#include "ProductionThreshold.h"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"

#include <fwk/CentralConfig.h>
#include <fwk/VModule.h>
#include <utl/TimeStamp.h>

#include <det/Detector.h>
#include <evt/Event.h>
#include <run/RunCollection.h>
#include <fwk/CentralConfig.h>
#include <utl/ErrorLogger.h>
#include <utl/Branch.h>

using namespace LuminanceSG;

ProductionThreshold::ProductionThreshold() : FTFP_BERT() {
}

ProductionThreshold::~ProductionThreshold() {
}

void ProductionThreshold::SetCuts()
{
	G4double CutForDifferentRegion=0.7*mm;
   	
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");	
		topBranch.GetChild("OtherElementsCut").GetData(CutForDifferentRegion);
	}

	SetDefaultCutValue(CutForDifferentRegion);

	SetCutsForBigTPCs();
	SetCutsForFTPC();
	SetCutsForGTPC();
	SetCutsForToFForward();
	SetCutsForToFLeftRight();
	SetCutsForPSD();
	SetCutsForBeamDetectors();
	SetCutsForGasBox();
	SetCutsForTarget();	
   	
	DumpCutValuesTable();		
}

void ProductionThreshold::SetCutsForBigTPCs()
{
		
	G4double BigTPCcutValue=0.7*mm;
   	G4double BigTPCcutValueElectron=0.7*mm;
	G4double BigTPCcutValuePositron=0.7*mm;
	G4double BigTPCcutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("BigTPCCut").GetData(BigTPCcutValue);
		topBranch.GetChild("BigTPCCutForElectrons").GetData(BigTPCcutValueElectron);
		topBranch.GetChild("BigTPCCutForPositrons").GetData(BigTPCcutValuePositron);
		topBranch.GetChild("BigTPCCutForGamma").GetData(BigTPCcutValueGamma);
	}

	G4String BigTPCRegionName;
	BigTPCRegionName="BigTPCregion";	

	G4Region* BigTPCregion = (G4RegionStore::GetInstance())->FindOrCreateRegion(BigTPCRegionName);

	G4ProductionCuts* BigTPCcut = new G4ProductionCuts();
	BigTPCcut -> SetProductionCut(BigTPCcutValue);
   	BigTPCcut -> SetProductionCut(BigTPCcutValueElectron,G4ProductionCuts::GetIndex("e-"));
	BigTPCcut -> SetProductionCut(BigTPCcutValuePositron,G4ProductionCuts::GetIndex("e+"));
	BigTPCcut -> SetProductionCut(BigTPCcutValueGamma,G4ProductionCuts::GetIndex("gamma"));
   	BigTPCregion -> SetProductionCuts(BigTPCcut);

}

void ProductionThreshold::SetCutsForFTPC()
{
	G4double FTPCcutValue=0.7*mm;
	G4double FTPCcutValueElectron=0.7*mm;
	G4double FTPCcutValuePositron=0.7*mm;
	G4double FTPCcutValueGamma=0.7*mm;

	
	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
			
		topBranch.GetChild("FTPCCut").GetData(FTPCcutValue);
		topBranch.GetChild("FTPCCutForElectrons").GetData(FTPCcutValueElectron);
		topBranch.GetChild("FTPCCutForPositrons").GetData(FTPCcutValuePositron);
		topBranch.GetChild("FTPCCutForGamma").GetData(FTPCcutValueGamma);
	}

	G4String FTPCRegionName;
    	FTPCRegionName = "FTPCregion";

	G4Region* FTPCregion = (G4RegionStore::GetInstance())->FindOrCreateRegion(FTPCRegionName);

	G4ProductionCuts* FTPCcut = new G4ProductionCuts();
	FTPCcut -> SetProductionCut(FTPCcutValue);
	FTPCcut -> SetProductionCut(FTPCcutValueElectron,G4ProductionCuts::GetIndex("e-"));
	FTPCcut -> SetProductionCut(FTPCcutValuePositron,G4ProductionCuts::GetIndex("e+"));
	FTPCcut -> SetProductionCut(FTPCcutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	FTPCregion -> SetProductionCuts(FTPCcut);

}

void ProductionThreshold::SetCutsForGTPC()
{
	G4double GTPCcutValue=0.7*mm;
	G4double GTPCcutValueElectron=0.7*mm;
	G4double GTPCcutValuePositron=0.7*mm;
	G4double GTPCcutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("GTPCCut").GetData(GTPCcutValue);
		topBranch.GetChild("GTPCCutForElectrons").GetData(GTPCcutValueElectron);
		topBranch.GetChild("GTPCCutForPositrons").GetData(GTPCcutValuePositron);
		topBranch.GetChild("GTPCCutForGamma").GetData(GTPCcutValueGamma);
	}


	G4String GTPCRegionName;
    	GTPCRegionName = "GTPCregion";

	G4Region* GTPCregion = (G4RegionStore::GetInstance())->FindOrCreateRegion(GTPCRegionName);

	G4ProductionCuts* GTPCcut = new G4ProductionCuts();
	GTPCcut -> SetProductionCut(GTPCcutValue);
	GTPCcut -> SetProductionCut(GTPCcutValueElectron,G4ProductionCuts::GetIndex("e-"));
	GTPCcut -> SetProductionCut(GTPCcutValuePositron,G4ProductionCuts::GetIndex("e+"));
	GTPCcut -> SetProductionCut(GTPCcutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	GTPCregion -> SetProductionCuts(GTPCcut);

}

void ProductionThreshold::SetCutsForToFForward()
{

	G4double tofForwardCutValue=0.7*mm;
	G4double tofForwardCutValueElectron=0.7*mm;
	G4double tofForwardCutValuePositron=0.7*mm;
	G4double tofForwardCutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("ToFForwardCut").GetData(tofForwardCutValue);
		topBranch.GetChild("ToFForwardCutForElectrons").GetData(tofForwardCutValueElectron);
		topBranch.GetChild("ToFForwardCutForPositrons").GetData(tofForwardCutValuePositron);
		topBranch.GetChild("ToFForwardCutForGamma").GetData(tofForwardCutValueGamma);
	}


	G4String tofScinRegionName;
	tofScinRegionName="scinForwardRegion";

	G4String tofLightGuideFishtailRegionName;
	tofLightGuideFishtailRegionName="tofLightGuideFishtailRegion";

	G4String tofLightGuideTubeRegionName;
	tofLightGuideTubeRegionName="tofLightGuideTubeRegion";

	G4Region* tofScinRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(tofScinRegionName);
	G4Region* tofLightFishtailRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(tofLightGuideFishtailRegionName);		
	G4Region* tofLightTubeRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(tofLightGuideTubeRegionName);

	G4ProductionCuts* tofScinCut = new G4ProductionCuts();
	tofScinCut -> SetProductionCut(tofForwardCutValue);
	tofScinCut -> SetProductionCut(tofForwardCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	tofScinCut -> SetProductionCut(tofForwardCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	tofScinCut -> SetProductionCut(tofForwardCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	tofScinRegion -> SetProductionCuts(tofScinCut);

	G4ProductionCuts* tofLightFishtailCut = new G4ProductionCuts();
	tofLightFishtailCut -> SetProductionCut(tofForwardCutValue);
	tofLightFishtailCut -> SetProductionCut(tofForwardCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	tofLightFishtailCut -> SetProductionCut(tofForwardCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	tofLightFishtailCut -> SetProductionCut(tofForwardCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	tofLightFishtailRegion->SetProductionCuts(tofLightFishtailCut);

	G4ProductionCuts* tofLightTubeCut = new G4ProductionCuts();
	tofLightTubeCut -> SetProductionCut(tofForwardCutValue);
	tofLightTubeCut -> SetProductionCut(tofForwardCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	tofLightTubeCut -> SetProductionCut(tofForwardCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	tofLightTubeCut -> SetProductionCut(tofForwardCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	tofLightTubeRegion -> SetProductionCuts(tofLightTubeCut);
	

}

void ProductionThreshold::SetCutsForToFLeftRight()
{
	G4double tofLeftRightCutValue=0.7*mm;
	G4double tofLeftRightCutValueElectron=0.7*mm;
	G4double tofLeftRightCutValuePositron=0.7*mm;
	G4double tofLeftRightCutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("ToFLeftRightCut").GetData(tofLeftRightCutValue);
		topBranch.GetChild("ToFLeftRightCutForElectrons").GetData(tofLeftRightCutValueElectron);
		topBranch.GetChild("ToFLeftRightCutForPositrons").GetData(tofLeftRightCutValuePositron);
		topBranch.GetChild("ToFLeftRightCutForGamma").GetData(tofLeftRightCutValueGamma);
	}	

	G4String tofScintLeftRightRegionName;
	tofScintLeftRightRegionName="scintLeftRightRegion";

	G4String tofPhotoLeftRightRegionName;
	tofPhotoLeftRightRegionName="photoLeftRightRegion";

	G4Region* tofScintLeftRightRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(tofScintLeftRightRegionName);
	G4Region* tofPhotoLeftRightRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(tofPhotoLeftRightRegionName);

	G4ProductionCuts* tofScintLeftRightCut = new G4ProductionCuts();
	tofScintLeftRightCut -> SetProductionCut(tofLeftRightCutValue);
	tofScintLeftRightCut -> SetProductionCut(tofLeftRightCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	tofScintLeftRightCut -> SetProductionCut(tofLeftRightCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	tofScintLeftRightCut -> SetProductionCut(tofLeftRightCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	tofScintLeftRightRegion->SetProductionCuts(tofScintLeftRightCut);

	G4ProductionCuts* tofPhotoLeftRightCut = new G4ProductionCuts();
	tofPhotoLeftRightCut -> SetProductionCut(tofLeftRightCutValue);
	tofPhotoLeftRightCut -> SetProductionCut(tofLeftRightCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	tofPhotoLeftRightCut -> SetProductionCut(tofLeftRightCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	tofPhotoLeftRightCut -> SetProductionCut(tofLeftRightCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	tofPhotoLeftRightRegion->SetProductionCuts(tofPhotoLeftRightCut);

}

void ProductionThreshold::SetCutsForPSD()
{
	G4double PSDCutValue=0.7*mm;
	G4double PSDCutValueElectron=0.7*mm;
	G4double PSDCutValuePositron=0.7*mm;
	G4double PSDCutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("PSDCut").GetData(PSDCutValue);
		topBranch.GetChild("PSDCutForElectrons").GetData(PSDCutValueElectron);
		topBranch.GetChild("PSDCutForPositrons").GetData(PSDCutValuePositron);
		topBranch.GetChild("PSDCutForGamma").GetData(PSDCutValueGamma);

	}

	G4String PSDRegionName;
	PSDRegionName="PSDRegion";

	G4String PSDdegraderRegionName;
	PSDdegraderRegionName="PSDdegraderRegion";	

	G4String PSDshortModuleRegionName;
	PSDshortModuleRegionName="PSDshortModuleRegion";

	G4Region* PSDRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(PSDRegionName);
	G4Region* PSDdegraderRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(PSDdegraderRegionName);
	G4Region* PSDshortModuleRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(PSDshortModuleRegionName);

	G4ProductionCuts* PSDCut = new G4ProductionCuts();
	PSDCut -> SetProductionCut(PSDCutValue);
	PSDCut -> SetProductionCut(PSDCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	PSDCut -> SetProductionCut(PSDCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	PSDCut -> SetProductionCut(PSDCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	PSDRegion -> SetProductionCuts(PSDCut);

	G4ProductionCuts* PSDdegraderCut = new G4ProductionCuts();
	PSDdegraderCut -> SetProductionCut(PSDCutValue);
	PSDdegraderCut -> SetProductionCut(PSDCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	PSDdegraderCut -> SetProductionCut(PSDCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	PSDdegraderCut -> SetProductionCut(PSDCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	PSDdegraderRegion -> SetProductionCuts(PSDdegraderCut);

	G4ProductionCuts* PSDshortModuleCut = new G4ProductionCuts();
	PSDshortModuleCut -> SetProductionCut(PSDCutValue);
	PSDshortModuleCut -> SetProductionCut(PSDCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	PSDshortModuleCut -> SetProductionCut(PSDCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	PSDshortModuleCut -> SetProductionCut(PSDCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	PSDshortModuleRegion -> SetProductionCuts(PSDshortModuleCut);

}

void ProductionThreshold::SetCutsForBeamDetectors()
{
	G4double BeamDetectorCutValue=0.7*mm;
	G4double BeamDetectorCutValueElectron=0.7*mm;
	G4double BeamDetectorCutValuePositron=0.7*mm;
	G4double BeamDetectorCutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("BeamDetectorCut").GetData(BeamDetectorCutValue);
		topBranch.GetChild("BeamDetectorCutForElectrons").GetData(BeamDetectorCutValueElectron);
		topBranch.GetChild("BeamDetectorCutForPositrons").GetData(BeamDetectorCutValuePositron);
		topBranch.GetChild("BeamDetectorCutForGamma").GetData(BeamDetectorCutValueGamma);	
	}

	G4String BeamDetectorName;
	BeamDetectorName="BeamDetectorRegion";

	G4Region* BeamDetectorRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(BeamDetectorName);

	G4ProductionCuts* BeamDetectorCut = new G4ProductionCuts();
	BeamDetectorCut -> SetProductionCut(BeamDetectorCutValue);	
	BeamDetectorCut -> SetProductionCut(BeamDetectorCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	BeamDetectorCut -> SetProductionCut(BeamDetectorCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	BeamDetectorCut -> SetProductionCut(BeamDetectorCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	BeamDetectorRegion->SetProductionCuts(BeamDetectorCut);

}

void ProductionThreshold::SetCutsForGasBox()
{

	G4double BoxCutValue=0.7*mm;
	G4double BoxCutValueElectron=0.7*mm;
	G4double BoxCutValuePositron=0.7*mm;
	G4double BoxCutValueGamma=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");
		
		topBranch.GetChild("GasBagCut").GetData(BoxCutValue);
		topBranch.GetChild("GasBagCutForElectrons").GetData(BoxCutValueElectron);
		topBranch.GetChild("GasBagCutForPositrons").GetData(BoxCutValuePositron);
		topBranch.GetChild("GasBagCutForGamma").GetData(BoxCutValueGamma);
	}

	G4String VertexBoxName;
	VertexBoxName="VertexBoxRegion";

	G4String VertexWindowDownName;
	VertexWindowDownName="VertexWindowDownRegion";

	G4String MainBoxName;
	MainBoxName="MainBoxRegion";

	G4String MainWindowDownName;
	MainWindowDownName="MainWindowDownRegion";

	G4Region* VertexBoxRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(VertexBoxName);
	G4Region* VertexWindowDownRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(VertexWindowDownName);
	G4Region* MainBoxRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(MainBoxName);
	G4Region* MainWindowDownRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(MainWindowDownName);

	G4ProductionCuts* VertexBoxCut = new G4ProductionCuts();
	VertexBoxCut -> SetProductionCut(BoxCutValue);
	VertexBoxCut -> SetProductionCut(BoxCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	VertexBoxCut -> SetProductionCut(BoxCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	VertexBoxCut -> SetProductionCut(BoxCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	VertexBoxRegion -> SetProductionCuts(VertexBoxCut);

	G4ProductionCuts* VertexWindowDownCut = new G4ProductionCuts();
	VertexWindowDownCut -> SetProductionCut(BoxCutValue);
	VertexWindowDownCut -> SetProductionCut(BoxCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	VertexWindowDownCut -> SetProductionCut(BoxCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	VertexWindowDownCut -> SetProductionCut(BoxCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	VertexWindowDownRegion -> SetProductionCuts(VertexWindowDownCut);

	G4ProductionCuts* MainBoxCut = new G4ProductionCuts();
	MainBoxCut -> SetProductionCut(BoxCutValue);
	MainBoxCut -> SetProductionCut(BoxCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	MainBoxCut -> SetProductionCut(BoxCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	MainBoxCut -> SetProductionCut(BoxCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	MainBoxRegion -> SetProductionCuts(MainBoxCut);

	G4ProductionCuts* MainWindowDownCut = new G4ProductionCuts();
	MainWindowDownCut -> SetProductionCut(BoxCutValue);
	MainWindowDownCut -> SetProductionCut(BoxCutValueElectron,G4ProductionCuts::GetIndex("e-"));
	MainWindowDownCut -> SetProductionCut(BoxCutValuePositron,G4ProductionCuts::GetIndex("e+"));
	MainWindowDownCut -> SetProductionCut(BoxCutValueGamma,G4ProductionCuts::GetIndex("gamma"));
	MainWindowDownRegion -> SetProductionCuts(MainWindowDownCut);
	
}

void ProductionThreshold::SetCutsForTarget()
{
	G4double TargetCut=0.7*mm;

	//Reading data from XML file
	fwk::CentralConfig& cc=fwk::CentralConfig::GetInstance();
	
	utl::Branch luminanceConfig=cc.GetTopBranch("Luminance");
        productionThresholdEnable = luminanceConfig.GetChild("physics").GetChild("physicsList").Get<std::string>();
	
	if(productionThresholdEnable == "FTFP_BERT_ProductionThreshold")
	{
		
		utl::Branch topBranch=cc.GetTopBranch("ProductionThreshold");

		topBranch.GetChild("TargetCut").GetData(TargetCut);
	}

	G4String TargetRegionName;
	TargetRegionName="TargetRegion";

	G4Region* TargetRegion = (G4RegionStore::GetInstance())->FindOrCreateRegion(TargetRegionName);

	G4ProductionCuts* TargetCut2 = new G4ProductionCuts();
	TargetCut2 -> SetProductionCut(TargetCut);
	TargetRegion -> SetProductionCuts(TargetCut2);

}	


