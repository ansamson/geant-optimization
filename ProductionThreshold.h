#ifndef _ProductionThreshold_h_
#define _ProductionThreshold_h_

#include "G4VModularPhysicsList.hh"
#include "G4StateManager.hh"
#include <iostream>
#include <CLHEP/Units/SystemOfUnits.h>

#include "globals.hh"
#include "G4VModularPhysicsList.hh"
#include "CompileTimeConstraints.hh"

#include "FTFP_BERT.hh"

namespace LuminanceSG {
	class ProductionThreshold : public FTFP_BERT
	{
	  public:
		ProductionThreshold(); 
		virtual ~ProductionThreshold();
		virtual void SetCuts();
	  protected:
		void SetCutsForBigTPCs();
		void SetCutsForFTPC();
		void SetCutsForGTPC();
		void SetCutsForToFForward();
		void SetCutsForToFLeftRight();
		void SetCutsForPSD();
		void SetCutsForBeamDetectors();
		void SetCutsForGasBox();
		void SetCutsForTarget();
		G4String productionThresholdEnable;	
	};
}



#endif
